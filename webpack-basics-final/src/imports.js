// We can export multiple objects, functions, or classes
export function makeTitle(text){
  let element = document.createElement('h1')
  element.innerHTML = text
  return element
}

// Since this is the DEFAULT export, we don't need to assign it to a variable
export default (element, root) => {
  if (root === undefined) root = document.body;
  root.appendChild(element)
}
