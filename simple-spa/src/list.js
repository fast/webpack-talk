import m from "mithril";

const ListItem = {
  view: ({ attrs: { item } }) =>
  m(".item.card.my-1", {
    onclick: e => m.route.set('/detail/:key', {key: item.id})
  }, m('.card-body', `${item.last}, ${item.first}`))
};

export default () => {
  let list = null;
  const getList = () =>
    window.scrollTo(0,0)
    m.request({
      url: "/api"
    }).then(r => (list = r));

  return {
    oninit: getList,
    view: () => (list && list.map(item => m(ListItem, { item })))
  };
};

