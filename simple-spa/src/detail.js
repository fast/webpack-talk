import m from 'mithril'

const Input = {
  view: ({attrs: {label, onchange, value}}) =>
    m('.form-group',
      m('label', label),
      m('input.form-control', {onchange, value})
    )
}

export default ({attrs, key}) => {
  let item = null;
  let error = null;
  let saved = null;

  const getItem = () => {
    window.scrollTo(0,0)
    m.request({
      url: `/api/${key}`
    }).then(r=>(item=r))
    .catch(e=>error='Failed to get item')
  }

  const saveItem = () => {
    console.log('saving')
    error = false
    m.request({
      url: `/api/${key}`,
      method: 'POST',
      body: item,
    }).then(r=>{
      item=r
      saved=new Date
    })
    .catch(e=>error='Failed to save item')
  }

  return {
    oninit: getItem,
    view: () => [
      item && m('div',
        m(Input, {
          label: 'First Name',
          value: item.first,
          onchange: e => item.first = e.target.value
        }),
        m(Input, {
          label: 'Last Name',
          value: item.last,
          onchange: e => item.last = e.target.value
        }),
        m(Input, {
          label: 'Email',
          value: item.email,
          onchange: e => item.email = e.target.value
        }),
        m('button.btn.btn-block.btn-outline-success', {onclick: saveItem}, 'Save'),
        error && m('.alert.alert-danger.mt-4', error),
        saved && m('div',
          m('.alert.alert-success.mt-4',
            {key: saved},
            'Saved Successfully!'
          )
        )
      )
    ]
  }
}
