import m from "mithril";
import List from "./list";
import Detail from "./detail";
import Layout from "./layout";


m.route(document.body, "/home", {
  "/home": {
    render: ()=>m(Layout, m(List))
  },
  "/detail/:key": {
    render: ({key})=>m(Layout, m(Detail, {key}))
  }
});

