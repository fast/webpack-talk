import m from "mithril";

export default {
  view: ({children}) => m('.main',
    children
  )
}
