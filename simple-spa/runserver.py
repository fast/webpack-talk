try:
    from http.server import SimpleHTTPRequestHandler, HTTPServer
except Exception:
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    from SocketServer import TCPServer as HTTPServer
import os
import json
import re

tempDB = [
    {
        'id': 1,
        'first': 'Jackson',
        'last': 'Imoff',
        'email': 'jimoff@fake-email.com',
    },
    {
        'id': 2,
        'first': 'George',
        'last': 'Park',
        'email': 'gpark@fake-email.com',
    },
    {
        'id': 3,
        'first': 'Sam',
        'last': 'Miller',
        'email': 'smiller@fake-email.com',
    },
    {
        'id': 4,
        'first': 'Charles',
        'last': 'Okey',
        'email': 'chokey@fake-email.com',
    },
]


def get_from_tempdb(get_id):
    sift = [x for x in tempDB if x['id'] == get_id]
    if len(sift) == 0:
        return None
    else:
        return sift[0]


class Handler(SimpleHTTPRequestHandler, object):
    def handle_headers(self, content_type='text/html', code=200):
        self.send_response(code)
        self.send_header('Content-type', content_type)
        self.end_headers()

    def do_GET(self):
        if self.path.startswith('/api'):
            if self.path == '/api':
                self.handle_headers('application/json')
                self.wfile.write(json.dumps(tempDB).encode('utf-8'))
            m = re.match(r"/api/(\d+)", self.path)
            if m is not None:
                get_id = int(m.group(1))
                item = get_from_tempdb(get_id)
                if item is None:
                    self.handle_headers(code=404)
                    self.wfile.write('Does not exist')
                else:
                    self.handle_headers('application/json')
                    self.wfile.write(json.dumps(item).encode('utf-8'))
            return
        super(Handler, self).do_GET()

    def do_POST(self):
        m = re.match(r"/api/(\d+)", self.path)
        if m is not None:
            get_id = int(m.group(1))
            item = get_from_tempdb(get_id)
            if item is not None:
                try:
                    length = int(self.headers.getheader('content-length', 0))
                except Exception:
                    length = int(self.headers.get('Content-Length'))
                file_data = self.rfile.read(length)
                try:
                    file_data = json.loads(file_data.decode('utf-8'))
                    del file_data['id']
                    item.update(**file_data)
                    self.handle_headers('application/json')
                    self.wfile.write(json.dumps(item).encode('utf-8'))

                except Exception:
                    self.handle_headers(code=403)
                return
        self.handle_headers(code=404)
        self.wfile.write('Does not exist')


HTTPServer.allow_reuse_address = True
server = HTTPServer(('127.0.0.1', 8000), Handler)
os.chdir('dist')
try:
    server.serve_forever()
except KeyboardInterrupt:
    pass
server.server_close()
