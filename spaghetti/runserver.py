from flask import Flask, render_template
from data import buckets
app = Flask(__name__)


# Our actual routes
@app.route('/')
def index():
    return render_template('index.html', buckets=buckets.list())


@app.route('/detail/<int:id>')
def detail(id):
    bucket = buckets.get(id)
    if bucket is not None:
        return render_template('detail.html', bucket=bucket)


if __name__ == "__main__":
    app.run(port=8000, debug=True)
