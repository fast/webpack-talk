class FakeDB:
    def __init__(self, data):
        self.data = data

    def list(self):
        return self.data

    def get(self, id):
        find = [x for x in self.data if x['id'] == id]
        if len(find) > 0:
            return find[0]

    def post(self, id, data):
        if id is None:
            return
        item = self.get(id)
        del data['id']
        item.update(**data)
        return item


buckets = FakeDB([
    dict(id=1, name='Fettucini', ingredients=['noodle', 'cream', 'cheese']),
    dict(id=2, name='Bolognese', ingredients=['noodle', 'meat', 'tomato']),
])

