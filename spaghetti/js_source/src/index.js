import m from "mithril";
import './editor.css';

const available_ingredients = [
  "noodle",
  "tomato",
  "meat",
  "cheese",
  "cream",
  "shrimp",
  "ketchup",
  "pistachios",
  "crunchy bits",
  "egg",
  "pepper",
  "salt",
]

const State = {
  bucket: window.context || {
    ingredients: []
  }
}


const Ingredient = {
  view: ({attrs: {text}}) =>
  m('.Ingredient', {
    onclick: () => State.bucket.ingredients.push(text)
  }, text)
}

const Available = {
  view: () => m('.Available',
    m('h4', 'Click to add to bucket'),
    available_ingredients.map(text=>m(Ingredient, {text}))
  )
}

const Bucket = {
  view: ()=>m('.Bucket',
    State.bucket.ingredients.map((item, i)=>
      m('.InBucket', {
        onclick: () => State.bucket.ingredients.splice(i, 1)
      }, item)
    )
  )
}

const App = {
  view: () => [
    m(Available),
    m(Bucket)
  ]
}

m.mount(document.getElementById('APP'), App)
