# Webpack + Your Backend Stack
This document will attempt to demonstrate "Clean" ways to integrate webpack & modern JS dev with non-js frameworks.

This tutorial assumes you're on a unix-like device with python installed (python 2 will do)

## Introduction

### What is Webpack?
Webpack lets you use the power of the nodejs javascript runtime to "transpile" the hell out of a bunch of pseudo-javascript garbage into something your browser can actually handle.

The internet decided that *everything* should be done in javascript, including css and html, but nobody actually wants to *use* javascript as it is universally accepted that is sucks.

So everyone and their dog went and made other languages, or pseudo-languages, or not-quite-css. Webpack lets you pack all that junk into real (albeit barely recognizable to the human eye) javascript.

### Why use Webpack?
Because some of the pseudo-languages are actually pretty cool. Modern declarative UI design has rapidly evolved via Vue, Angular and React. TypeScript came along and made javascript palatable to *bad programmers* who can't live without strong typing.

Webpack also lets you jampack anything you find on NPM (nodejs equivalent of python's pip or ruby's rake) into your transpiled output, giving you access to hundreds of thousands of javascript libraries without needing to steal stuff via "view source code".

Ultimately it is a tool I recommend using *when necessary*. When you're building something that has a lot of moving parts, but relatively few "views". Regular javascript is getting better all the time -- but when you need to do something *hard*, and your javascript is stretching into the thousands of lines, or you feel like a more modern tool might help your code run smoother, it might be time to "webpack" it.


### How to use Webpack
In my experience Webpack's "best practices" seem to change every three hours. There's a lot of bits and pieces that move to make webpack work, so the "best" way to do things seems to be a bit of a toss up. I'm going to be borrowing a lot from webpack's main website here, meaning this guide might be a bit out of date by the time I'm done writing it.

Let's go through a *barebones* webpack example which will do the following:

 - Compile down our javascript files into a single file
 - Generate an HTML file that links to the created js file
 
 There's a million other things you can do beyond this -- you can serve your generated file, hot-reload your changes on the fly, install strict linters that ensure your code is up to snuff -- but we're going to keep it simple here.

Install nodejs and npm. You may need to google this bit
Create a project directory and initialize a project:

```bash
mkdir webpack-basics && cd webpack-basics;
npm init
#You'll need to fill some stuff in here or hit enter a lot
npm install -D webpack webpack-cli
```
This will install webpack and the commandline tools, and about 400 other packages that we'll hope stay maintained.


In order to actually *use* webpack (without typing the full path to the binary) we need to add it to this npm project's `package.json` as a script. So edit that:

**package.json**
```json
{
  "name": "webpack-basics",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "prod": "webpack -p",
    "dev": "webpack --watch -d"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "webpack": "^4.42.0",
    "webpack-cli": "^3.3.11"
  }
}
```
We're creating the scripts: **prod** and **dev** using some basic webpack flags. Dev-mode will watch changes to files and re-transpile on the fly for us, while prod will produce minified code.

Webpack's also going to want a config file. There's all kinds of ways to go about this, the simplest is to make a `webpack.config.js` in the root of the project.

**webpack.config.js**
```js
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  }
};
```

We've told webpack "Go find src/index.js, compile it and it's requirements into "dist/app.js". Simple. Now lets add some js to compile:

**src/index.js**
```js
// Importing the default export
// AND a non-default export from src/imports.js
import RenderDom, {makeTitle} from './imports'
RenderDom(makeTitle("Hello World!"))
```
**src/imports.js**
```js
// We can export multiple objects, functions, or classes
export function makeTitle(text){
  let element = document.createElement('h1')
  element.innerHTML = text
  return element
}

// Since this is the DEFAULT export, we don't need to assign it to a variable
export default (element, root) => {
  if (root === undefined) root = document.body;
  root.appendChild(element)
}
```

Let's compile this bad-boy with
```bash
npm run prod
```

This will create ``dist/app.js`` for us, but no html to view it. We're going to use a *very* popular webpack plugin to auto-create an index.html with links to our generated app:

```bash
npm install -D html-webpack-plugin
```
**webpack.config.js**
```js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // Where we'll start our transpiling
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Our Special App'
    })
  ]
};
```
This time let's continously compile in dev mode:
```bash
npm run dev
```

If you open the newly created ``dist/index.html`` in your browser you can see our excellent application at work. Since we're starting webpack in watch mode, you can also edit the stuff in ``src/`` and your javascript files will recompile automatically, allowing you to refresh your browser to see changes.

> **NOTE:** There's all kinds of ways to make your browser auto-refresh too, but there's about 10-million ways to do it, so we're not going to get into that here.

With that we've got a barebones basic webpack setup that can be built upon. 


## Simple Case: The Single Page Application

The easiest place to use webpack is when you're going to build a simple single-page application.

In these cases your JS App will handle pretty much all of the user interface, *including routing*, while your backend handles everything else. The frontend and backend will do most (if not all) communication through API endpoints provided by the backend.

> **NOTE:** Even if you aren't planning on building an SPA, we'll be doing some things in this section such as bundling CSS assets and splitting chunks in order to minimize page reload.

Download the git repo for this tutorial (if you haven't already) and open up the ``simple-spa`` folder:

```bash
git clone https://git.uwaterloo.ca/fast/webpack-talk.git
cd webpack-talk/simple-spa
# Install the packages
npm install
npm run prod
python runserver.py
```

This will compile a small mithril frontend, and run a small python backend server at http://localhost:8000 -- the server's nothing fancy, and just uses python's builtin simplehttp server to:

- serve everything in the ``dist`` folder
- present a couple of ``/api`` endpoints to get and post JSON data to

The frontend isn't much to shake a stick at either -- a lame list that you can click, edit, save, and go back. However, as I've demonstrated by using hashes for routing, *the user never actually leaves the single page once loaded*.

If you decide to go the SPA route, (or even not) there's some more configurations we can make to webpack (generally pretty standard ones) to improve our code output and add some features. Kill your server and let's get cracking.

### Including CSS Libraries
If you're writing a single page application, you probably want to keep *all* of your frontend stuff handled by webpack, including styling!

Inside the ``src/`` folder I've included a basic css file that sets the background color -- we can just import that directly into our  ``src/index.js``

**src/index.js**
```js
...

import "./main.css";

m.route(...
```

If we try to build our code with ``npm run prod`` or ``npm run dev`` we'll get an error from webpack, telling us *it doesn't know how to handle this file*. So we'll do the logical thing and install ten-thousand dependencies to include css in our tooling:

> **NOTE** it's actually more like 16 dependencies

```bash
npm install -D style-loader css-loader
```
Webpack uses a thing called a **loader** to figure out how to deal with certain filetypes. It knows how to handle standard javascript pretty well,
but if we want to handle anything else (css, jsx, fancy es2017+ stuff) we'll need to specify a loader.

Let's use a regex rule to tell webpack how to handle any files with a filename ending in .css with our new installed style loader:
**webpack.config.js**
```js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // Where we'll start our transpiling
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },
  // NEW STUFF HERE
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'SPA'
    })
  ]
};
```

This is a common pattern when you start adding the *really* weird pseudo-javascript projects and things like react.

Let's fire up the compiler and the server:
```bash
npm run prod && python runserver.py
```

*Looks good*

Problem is writing CSS is *hard*, not hard like writing code is hard, but who's going to write 10k lines of css to make websites actually look good? Bootstrap did, so let's use their stuff.

```bash
npm install -D bootstrap
```

**src/index.js**
```js
...

import "./main.css";
import "bootstrap/dist/css/bootstrap.min.css";

m.route(...
```

Re-run our renderer and *lo and behold* -- we're strapped in!

> **NOTE:** This is the simplest possible way to import bootstrap's stylesheets -- if you need more control you can import their SCSS files instead and modify the way their base components look and feel. Check out more at [the official bootstrap+webpack docs](https://getbootstrap.com/docs/4.4/getting-started/webpack/#importing-precompiled-sass)


### Splitting Your Code
Now at the moment *all* of our code is getting pumped out into a single app.js -- the CSS, the JS, and all of our plugins (bootstrap stuff, mithril, etc) is all getting crammed into a single file. This is convenient for small apps, but when you're wielding giants like react & redux you'll quickly climb into the megabytes and start to notice lag and pop-in.

The easiest fix we can make is to separate out our stylesheets -- right now any css we import is going to be injected at the head of our document *after* our JS loads. To make separate CSS files, we simply need **more dependencies**:

```bash
npm install mini-css-extract-plugin
```
Yes, the plugins are starting to sound a little less official, but this one's officially sanctioned right on the Webpack website. Now let's do some more modification of our lovely little config:

**webpack.config.js**
```js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// NEW
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  // Where we'll start our transpiling
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        // CHANGED
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'SPA'
    }),
    // NEW
    new MiniCssExtractPlugin()
  ]
};
```
Now instead of using the style-loader which has javascript inject CSS for us, the lovely MiniCssExtractPlugin will do it for us. If you recompile and look at the source code of ``dist/app.js`` you'll see that our other plugin automatically picked up and injected the css and js files for us! Nice!


### Avoiding the Cache issue
Since everybody's constantly downloading half a gig everytime they load facebook nowadays, browsers got smart and started *caching* things for us. That's great!

Since we're using my shitty server and serving on localhost you probably won't notice in, but in production when your users are loading up your app.js it'll get cached. Then, when you recompile it and re-deploy you'll users running an older version of your JS... and I've found via the complaints of users that "just hold shift and refresh to clear the cached javascript" isn't a valid solution when my website randomly breaks on them.

Fortunately there's a really easy solution -- in the olden days guides would recommend compiling your filenames with hashes in them -- this would mean you're builing ``app-42134876dfsa78fd.js``, deleting the old file, and loading that up. While this was well and good, a simpler solution has been determined, which is to append a GET query to the end of your static file urls, such as ``app.js?32412kjh23k4h2h``. We don't need to continuously rename our files, and our browsers will know when they need to load a new version of the JS!

Our HtmlWebpackPlugin can handle this for us automatically:
**webpack.config.js**
```js
...
new HtmlWebpackPlugin({
    title: 'SPA',
    hash: true
})
...
```
If you recompile and view source you'll see the difference if you view the source code generated in ``dist/index.html``, and you'll be able to avoid a -lot- of headaches. 

### Compiling Harder with Babel *(optional)*
If you take a look at some of the js files in the project, you may notice they use some funky notation in some cases. This is because even javascript thought javascript sucked, so they added a whole bunch of new syntax sugar, some of which is great!

Then not all of the popular browsers implemented all the things. We're getting there, but not quite. As webdevs we *occasionally* need our apps to be compatible with older browsers, or have a userbase that's not great at updating their computers.

If we want to cover our bases, it's best to transpile our source code even farther into universally understandable (by computers, it'll still be unreadable to us) javascript. This is also the case if you end up using some of the new-fangled language syntaxes like JSX while using react.

Babel is the de-facto tool to use for this, and comes with it's own mountain of dependencies -- let's get a -basic- configuration going so our final app.js is as widely supported as possible:

```bash
npm install -D babel-loader @babel/core @babel/preset-env
```

And configure babel to handle all of our *non installed* packages by adding a rule for js files:
**webpack**
```js
...
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      // NEW
      {
        test: /\.js$/i,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
...
```

Now all JS will get passed through babel -- the preset-env is babel's current "sane" defaults for transforming code to a realistic subset of browsers. While it isn't mandatory to include babel in your projects, it -will- be required to use certain languages or frameworks (react), or if you want to use bleeding edge JS features (object rest spread, pretty arrow functions, fetch), or even if you want to be 100% sure your JS output will support a *specific* browser.

Configuring babel and all of it's options is a journey of its own ([here's a list of official plugins, for example](https://babeljs.io/docs/en/plugins)), so we'll keep this baseline configuration for now and move forward!

> **NOTE**: If you're going to *seriously* start building an SPA, take a look at the getting started guides for some of the major frameworks out there like Vue or React. They frequently come with fully usable getting started guides or special commands that will help you eliminate needing to do much configuring until you're further down the road. What we've done here is *very* general-purpose, and can be expanded, for example, to export Vue AND React with a single webpack config.

## Advanced Mode: Adding Webpack to Existing Spaghetti Code

Let's examine a more realistic case:

You have an application that's been around for a while, primarily in an MVC framework. You've got a page that has some pretty heavy javascript on it that's just a *bit* too janky and you want to overhaul it using the hottest new JS. You *don't* want to rig up a whole API, you just want to upgrade the one page, and add more spaghetti to the bucket.

This case will assume the following:
1. Your backend framework doesn't have native JS transpiling or built-in webpack
2. Your backend framework has a *templating language* for rendering HTML content
3. You want to avoid installing additional "bridging" packages (we've already installed half the internet as it is)

We can make this work (smoothly and consistently) by using everything we've got installed by the end of the **Introduction**, that is webpack, webpack-cli and the http plugin, however we'll be starting in this case with a config similar to the one at the end of the SPA case.

Make sure you've download the git repo for this project and go into the ``spaghetti`` folder.

What I've done here is made a minimal Flask program which should work in python2.7 or 3+ You'll need pip installed for this one to work, but all things considered I've tried to keep it -very- minimal.
``In spaghetti folder``
```bash
# for python 3
python -m venv env
# python 2
virtualenv env

source env/bin/activate
pip install --upgrade pip
pip install Flask blinker
python runserver.py
```
Go to your localhost:8000

Clicking on one of the spaghetti buckets takes us to what we assume will be some arbitrarily complex page with some form of javascript interaction going on. The landing page, on the other hand, is simply a template rendered through Flask's templating framework.

Take a quick look at the spaghetti project folder. In ``runserver.py`` we have 2 routes, both of which return templates (index.html and detail.html)

Both of those templates are super basic -- they inherit from base.html, then have some html in the body block -- many templating languages share similar features, so your framework of choice should have some analog to this setup

In the js_source we have our now familiar src file, package.json and webpack config. In this folder you should run ``npm install`` to install the required packages. Let's get going:

### The Goal
Our goal is to compile our small app, and inject it into the ``detail.html`` file -- that means we need our css in the head, and js in the bottom. If, for whatever reason, our app generated several optimized chunks (multiple js files), we'd want to make sure those *all* end up in here.

### The Naive approach (i.e. the first time I tried this)
The simplest thing to do here would be to compile our JS files (in this case editor.js would be our output) and import them by hand with a script tag. This will work, but you'll run into the caching issue we mentioned above.
Instead, we're going to need a way for our templating language to *know* what the hashes are from our compiled output.

### The Package Approach
Webpack is popular as all hell nowadays, and unless your backend is written in brainfuck or haxe you *probably* can find a pre-built package that helps integrate your webpack into your templates. I mean why not, we've already installed like 800 just to transpile and pack up a mundane javascript file

This path has its own set of flaws. Pretty much every plugin for webpack has a configuration guide the size of the Old Testament, and as soon as you need to stray off the beaten path you'll be neck deep in psalms trying to figure out how to hammer the plugin into working.

I've also personally had  the fun experience of my python plugin no longer getting updated and no longer working with the webpack plugin it was supposed to talk to.

Realistically, if your framework is well supported, there will be a well supported solution that can get you through connecting your webpack to your templates in an easy enough manner.

But that's not what you came here for, we want **spaghetti**.

### The Integrated Approach
Nothing helps you understand what's going on like re-inventing the wheel, and writing an integration layer isn't too hard. 
If you run dev or prod right now, we have a config that will generate ``static/compiled/editor.js`` and ``templates/compiled/editor.html``.

Let's get started!

> **NOTE:** From this point on we're getting into uncharted areas -- there's countless ways to do what I'm about to do, and nothing after this point can be considered standard or best practice. Up until here the advice has been fairly solid, and provides a good basis for beginning with webpacks. Here be dragons.

#### Step 1: Understand your Framework's static files
In order to do this, we're going to need to know how our template language works. Most frameworks will have something in place for handling static files. In flask, the url to a static file (anything in the ``static/`` directory) with the following:
```
{{ url_for('static', filename='path/to/file') }}
```
We're going to need this to build a meta template.

In our case we're also going to use flask templating's "import" function, which lets us import other html templates, and if statements to only import specific blocks.

#### Step 2: Configure our meta-template
What we effectively want to do here is replace the default template the webpack html plugin uses with something that fits our templating engine. the webpack html template plugin uses "ejs" for its templating, so we'll be mixing flask's jina with ejs to do something along the lines of:
```
JINJA: If css:
	EJS: for css in css chunk..
		JINJA: static url + ? EJS: hash
JINJA: else
	EJS: for js in js chunk..
		JINJA: static url + ? EJS: hash
```

If you're interested in seeing this eldrich horror take a look in ``js_source/template.ejs``

> **NOTE:** This will look much more sane once the templates are compiled

All we need to do is tell the html webpack plugin to use this template instead of the default one, and disable the default injection logic, as our new template handles it manually:

**In js_source/webpack.config.js**
```js
...
    new HtmlWebpackPlugin({
      hash: true,
      filename: `../../templates/compiled/editor.html`,
      template: 'template.ejs',
      inject: false
    }),
```

If you run npm run dev or prod, you'll end up with something like this:
**templates/compiled/editor.html**
```html
{% if css %}
<!-- TRANSPILED CSS -->

{% else %}
<!-- TRANSPILED JS -->

<script src="{{ url for('static' filename='../../static/compiled/editor.js') }}?022a23f149e333586dc3"></script>

{% endif %}
```
... Which isn't *quite* right -- notice the file path is relative to our index file! Flask wants static paths to be relative to the ``static/`` folder, so let's tell webpack to alter the "publicPath" of our js output:

**In js_source/webpack.config.js**
```js
...
  output: {
    path: path.resolve(__dirname, '../static/compiled'),
    publicPath: 'compiled/',
    filename: `editor.js`
  },
```

After this our compiled template is looking good -- now we just need to -use- the files.

#### Step 4: Use the compiled templates
The way this project's templates are set up, we can put content into the blocks "head" and "js" -- lets load the parts of our generated html file!:

**templates/detail.html (add to bottom of file)**
```html
{% block js %}
  {% include 'compiled/editor.html' %}
{% endblock %}

{% block head %}
  {% with css=True %}{% include 'compiled/editor.html' %}{% endwith %}
{% endblock %}
```

Make sure everything is compiled and your server is running on port 8000, then go have a look at our pasta bucket editor!

### Passing Context on the Sly
You may have noticed that the pasta bucket editor doesn't actually show the stuff that was *originally* in the bucket. If we were writing a full SPA we'd want to create API endpoints to act as getters and setters.

Since we're already able to easily pass context between our backend and frontend, we should leverage that at least for the getting.

My personal trick for getting this done is adding a global function to my context processing while compiling templates -- this function should take any variables passed to templates called "js_context" and convert it into JSON. We can then make sure our base template sets window.context = js_context, and window.context will then be available to our JS.

In flask this is *very* easy, but in your framework this may require additional context processing functions:

**templates/base.html**
```html
...
    <!-- JAVASCRIPT -->
    
    <!-- ADD THIS LINE --!>
    <script>window.context={{js_context|default({}, true)|tojson}}</script>

    {% block js %}
    {% endblock %}
  </body>
...
```

Now in any view that renders a template which inherits from base, we can simply set "js_context", and those variables (as long as they're json compatible) will be accessible through ``window.context`` in our JS!

The bucket editor is already configured to load from context -- so now we just need to add js_context to our view:

**runserver.py**
```python
@app.route('/detail/<int:id>')
def detail(id):
    bucket = buckets.get(id)
    if bucket is not None:
        return render_template('detail.html', bucket=bucket, js_context=bucket)
```

Now you can edit the existing buckets! Adding a POST would still need a new endpoint, but that endpoint simply needs to parse and validate the posted JSON we're editing on the page, so we won't cover it here.

## Finishing Points
From here you should be able to use your imagination to see how to integrate a similar strategy in your existing apps -- this is a relatively simplistic integration, but its simplicity lends to a very useful level of configuration flexibility. Without needing to dig into 30 pages of how to configure some flask-webpack-interop plugin that may eventually not cover that *one* edge case you need, you can use these building blocks to configure any kind of interop with an existing templating language.

As an exercise, try modifying template.ejs and the webpack config to integrate with your favorite framework's templating engine!

### Multiple outputs


### To commit or not to commit


